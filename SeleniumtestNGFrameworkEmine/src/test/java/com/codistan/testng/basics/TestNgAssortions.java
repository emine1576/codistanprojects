package com.codistan.testng.basics;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class TestNgAssortions {
	public WebDriver driver;
	
  @BeforeTest
  public void setUpTest() {
	  WebDriverManager.chromedriver().setup();
	  driver = new ChromeDriver();
	  driver.get("https://www.amazon.com");
	  
	  System.out.println();
	  
	  
  }
@Test(description="assertEquals")
public void assertEquals() {
	
	String actualTitle = driver.getTitle();
	System.out.println(actualTitle);
	
	String expectedTitle = "amazon";
	Assert.assertEquals(actualTitle, expectedTitle);
	
	}
 @Test
 public void assertNotEquals() {
	 String actualUrl = driver.getCurrentUrl();
	 String expectedUrl ="http";
	Assert.assertNotEquals(actualUrl, expectedUrl,"Url test results");
 }
 @Test(description ="assertTrue")
 
 public void assertTrue() {
	 String expectedTitle ="amazon";
	 Assert.assertTrue(expectedTitle.equals(driver.getTitle()),"Expected title check point" );
	 
	 
 }
 
 @Test
 public void assertFalse() {
	 String expectedTitle = "amzn";
	 Assert.assertFalse(expectedTitle.equalsIgnoreCase(driver.getTitle()),"Expected title negative test");
 }
}

	
	


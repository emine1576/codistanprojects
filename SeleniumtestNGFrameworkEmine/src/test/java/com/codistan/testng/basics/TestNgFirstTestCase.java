package com.codistan.testng.basics;




import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

@Test
public class TestNgFirstTestCase {
	public WebDriver driver;
	
  @BeforeTest(alwaysRun =true)
  public void bTest() {
	  WebDriverManager.iedriver().setup();
	 WebDriverManager.chromedriver().setup();
	  WebDriverManager.firefoxdriver().setup();
	  
	  
	  
	  
  }
  
  @Parameters("url")
  public void chromeTest(String url) {
	  WebDriverManager.chromedriver().setup();
	  driver = new ChromeDriver();
	  //if i declare webdriver current method here , it will just visible for 
	  driver.manage().window().maximize();
	  driver.get("https://www.amazon.com");
 
 
	  
	  
  }
  public void fireFoxTest() {
	  WebDriverManager.firefoxdriver().setup();
	  driver = new FirefoxDriver();
	  driver.get("https://www.ebay.com");
	  
  }
  
 // @Parameters("url")
  //description also is added to html report.
  public void iETest() {
      WebDriverManager.iedriver().setup();
      driver = new InternetExplorerDriver();
      driver.manage().window().maximize();
      driver.get("https://www.amazon.com");
  
  }
	
	
	@AfterMethod
	@AfterTest()
  public void tearDown() {
	  
  }
}



package com.codistan.testng.basics;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.testng.Assert;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;



import io.github.bonigarcia.wdm.WebDriverManager;


public class TestNGprac {
	
	public WebDriver driver;
	@Test
	@Parameters({"url","APIKey/username"})

public void testNg(String urlName, String key) {
	
		 WebDriverManager.chromedriver().setup();
		 driver.get("http://newtours.demoaut.com/");
		 java.lang.String PageSource ;
		PageSource= driver.getPageSource();
		
		
	
WebElement userName = driver.findElement(By.name("userName"));
WebElement password = driver.findElement(By.name("password"));
WebElement signinButton = driver.findElement(By.name("login"));
userName.sendKeys("tutorial");
password.sendKeys("tutorial");
signinButton.click();
		 

}
@FindBy(xpath ="//input[@name='userName']")
public WebElement userName;

@FindBy(xpath ="//input[@name='password']")
public WebElement passWord;

@FindBy(xpath ="//input[@name='login']")
public WebElement clickButton;
public void logIn(String userN, String passW) {
	
	userName.sendKeys("tutorial");
	passWord.sendKeys("tutorial");
	clickButton.click();
}
	




	 @Test(description="assertEquals")
	 public void assertEquals() {
	 	
	 	String actualTitle = driver.getTitle();
	 	System.out.println(actualTitle);
	 	
	 	String expectedTitle = "NewTour";
	 	Assert.assertEquals(actualTitle, expectedTitle);
	 	

}
	 
}
//10.Enter user name and password as "tutorial" and click signin button
//11.Verify successful login by checking if roundtrip radio button is selected
//12.Create a webelement for oneway radio button and click 
//13. Verify and print if it is selected.
package com.codistan.testng.basics;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class TestNGbasics {
	@BeforeTest
	public void beforeTest() {
		System.out.println("BeforeTest: I will run before my test only one time");
		
	
	}
	@BeforeMethod
	public void beforeMethod() {
		System.out.println("BeforeMethod vs beforeTest runs before ever test case");
	}
	
	@Test
	public void testOne() {
		System.out.println("Test1: This will run as Test One");
	
		
	}
	@Test
	public void testTwo() {
		System.out.println("Test2:This will run as Test Two");
		
		
		
	}
	@Test
	public void testThree() {
		System.out.println("Test3:This will run as Test Three");
	}
	@AfterMethod
	public void afterMethod() {
		System.out.println("AfterMethod vs AfterTest");
		
	}
	
	@AfterTest
	public void afterTestCase() {
		System.out.println("I will run after my test");
		
	
	
}
}
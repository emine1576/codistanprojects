package com.codistan.apitesting;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;

import static io.restassured.RestAssured.*; 
import static io.restassured.matcher.RestAssuredMatchers.*; 
import static org.hamcrest.Matchers.*;

public class PetStoreTests {
	@BeforeClass
	public void setup() {
	RestAssured.baseURI =("https://petstore.swagger.io/v2");
	RestAssured.basePath ="/pet";
	}
@Test
public void getPetPositive() {
	given().when().get("/11303741")
	.then().log().all()
	.statusCode(200);
	
}
@Test
public void getPetPositiveWithoutGiven() {
	
	when()
	.get("/11303741")
	.then()
	.log().all()
	.statusCode(200);
	

}
@Test
public void getPetPositiveWithLog() {
	
	when()
	.get("/11303741")
	.then()
	.log().all()
	
	.statusCode(200)
	.and().body("id", equalTo(11303741));
	
	
	
	
	
	
	
	
	
}

@Test
public void getPetPositiveWithStatusCode() {
	
	when()
	.get("/201910")
	.then()
	.log().all()
	
	.statusCode(200)
	.and().body("name", equalTo("cutie"))
	.and().body("id", equalTo(201910));
	
	
	
}
@Test
public void getPetNegativeWithStatusCode() {
	when()
		.get("/11303741")
	.then()
		.log().all()
		.statusCode(not(equalTo(404)));
}
	
@Test
public void postPetPositive() {
	given()
	.when()	
		.contentType(ContentType.JSON)
		.body("{\r\n" + 
			"  \"id\": 201910,\r\n" + 
			"  \"category\": {\r\n" + 
			"    \"id\": 0,\r\n" + 
			"    \"name\": \"string\"\r\n" + 
			"  },\r\n" + 
			"  \"name\": \"cutie\",\r\n" + 
			"  \"photoUrls\": [\r\n" + 
			"    \"string\"\r\n" + 
			"  ],\r\n" + 
			"  \"tags\": [\r\n" + 
			"    {\r\n" + 
			"      \"id\": 0,\r\n" + 
			"      \"name\": \"string\"\r\n" + 
			"    }\r\n" + 
			"  ],\r\n" + 
			"  \"status\": \"available\"\r\n" + 
			"}")
		.post()
	.then()
		.log().all()
		.statusCode(200)
		.and()
		.body("id", equalTo(201910)); 
}

@Test
public void putPetStore() {
	given()
	  .contentType(ContentType.JSON)
	.when()
	 .body("{\r\n" +
		  		"  \"id\": 2909,\r\n" +
		  		"  \"category\": {\r\n" +
		  		"    \"id\": 0,\r\n" +
		  		"    \"name\": \"string\"\r\n" +
		  		"  },\r\n" +
		  		"  \"name\": \"doggie\",\r\n" +
		  		"  \"photoUrls\": [\r\n" +
		  		"    \"string\"\r\n" +
		  		"  ],\r\n" +
		  		"  \"tags\": [\r\n" +
		  		"    {\r\n" +
		  		"      \"id\": 0,\r\n" +
		  		"      \"name\": \"string\"\r\n" +
		  		"    }\r\n" +
		  		"  ],\r\n" +
		  		"  \"status\": \"available\"\r\n" +
		  		"}")
	  .put()
	.then()
	//log all shows all the info with JSON code
	  .log().all()
	  .statusCode(200)
	  .and().body("name",equalTo("doggie"));
}
@Test(dependsOnMethods = {"putPetStore"})
public void deletePetStore() {
	given()
	.when()
	  .delete("/2909")
	.then()
	//log all shows all the info with JSON code
	  .log().all()
	  .statusCode(200);
}

}


package com.codistan.apitesting;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.equalTo;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.coditan.utility.ApiUtils;
import com.coditan.utility.PropertyConfig;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;
import io.restassured.response.Response;

public class JiraEndt2End {
//First we need to create a ticket
//Assign to ticket somebody
//Update a comment
//Completing jira ticket
//deleting the jira ticket
	String newJiraCardKey = "";
	@BeforeClass
	public void setupJIRATests() {
		RestAssured.baseURI = "https://codistan201910a.atlassian.net/rest/api/2";
		RestAssured.basePath = "/issue"; 
		RestAssured.baseURI = PropertyConfig.getProperty("uri");
		RestAssured.basePath = PropertyConfig.getProperty("path.issue"); 
	}
	
	
	
	@Test(enabled = false)
	public void createJiraCard() {
	newJiraCardKey = given()
		.auth().preemptive().basic("codistan.automation@gmail.com", "HkZjInsNvyYudWVc02z7EE64")
		.contentType(ContentType.JSON)
		.body("{\r\n" + 
				"  \"update\": {\r\n" + 
				"    },\r\n" + 
				"  \"fields\": {\r\n" + 
				"    \"summary\": \"Rest Assured Automated API Testing\",\r\n" + 
				"      \"issuetype\": {\r\n" + 
				"      \"id\": \"10000\"\r\n" + 
				"    },\r\n" + 
				"    \"customfield_10011\": \"Test\",\r\n" + 
				"\r\n" + 
				"    \"project\": {\r\n" + 
				"      \"id\": \"10000\"\r\n" + 
				"    },\r\n" + 
				"    \"description\": \"Testing JIRA Creation\"\r\n" + 
				"\r\n" + 
				"\r\n" + 
				"  }\r\n" + 
				"}")
	.when()
		.post("")
	.then()
		.log().all()
		.statusCode(201).extract().path("key"); 
	
	}
	
	
	
	@Test(enabled = true)
	public void createJiraCardTwo() {
		String payload = "{\r\n" + 
				"  \"update\": {\r\n" + 
				"    },\r\n" + 
				"  \"fields\": {\r\n" + 
				"    \"summary\": \"Rest Assured Automated API Testing\",\r\n" + 
				"      \"issuetype\": {\r\n" + 
				"      \"id\": \"10000\"\r\n" + 
				"    },\r\n" + 
				"    \"customfield_10011\": \"Test\",\r\n" + 
				"\r\n" + 
				"    \"project\": {\r\n" + 
				"      \"id\": \"10000\"\r\n" + 
				"    },\r\n" + 
				"    \"description\": \"Testing JIRA Creation\"\r\n" + 
				"\r\n" + 
				"\r\n" + 
				"  }\r\n" + 
				"}";
	newJiraCardKey = ApiUtils.createJiraTicket(payload)
	.then()
		.log().all()
		.statusCode(201).extract().path("key"); 
	
	}
	
	@Test(dependsOnMethods = "createJiraCard",enabled = true)
	public void getJIRACardPositive() {
		given()
			.auth().preemptive().basic("codistan.automation@gmail.com", "HkZjInsNvyYudWVc02z7EE64")
		.when()
			.get("/" + newJiraCardKey)
		.then()
			.log().all()
			.statusCode(200)
			.and()
			.body("fields.summary", equalTo("Rest Assured Automated API Testing"))
			.and()
			.body("fields.description", containsString("Testing JIRA"))
			.and()
			.body("key", equalTo(newJiraCardKey))
			.and()
			.body("fields.reporter.displayName", equalTo("Codistan Training")); 
	}
	
	@Test(dependsOnMethods = "createJiraCard",enabled = false)
	public void getJIRACardPositiveTwo() {
		Response responseObj = 
			given()
				.auth().preemptive().basic("codistan.automation@gmail.com", "HkZjInsNvyYudWVc02z7EE64")
			.when()
				.get("/" + newJiraCardKey);
		
		responseObj.then().statusCode(200);
		
		System.out.println(responseObj.getBody().path("key"));

	}
	


}


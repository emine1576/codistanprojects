package com.codistan.apitesting;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import io.restassured.RestAssured;
import io.restassured.http.ContentType;

import static io.restassured.RestAssured.*; 
import static io.restassured.matcher.RestAssuredMatchers.*; 
import static org.hamcrest.Matchers.*;

public class JiraTest {
	@Test
	public void getJIRACardPositive() {
		given()
			.auth().preemptive().basic("codistan.automation@gmail.com", "HkZjInsNvyYudWVc02z7EE64")
		.when()
			.get("https://codistan201910a.atlassian.net/rest/api/2/issue/AV-263")
		.then()
			.log().all()
			.statusCode(200)
			.and()
			.body("fields.summary", equalTo("Main order flow broken"))
			.and()
			.body("fields.description", containsString("Testing JIRA"))
			.and()
			.body("key", equalTo("AV-263"))
			.and()
			.body("fields.reporter.displayName", equalTo("Codistan Training"));
			
			
	
		
			
			
	}
	
			
			
				
				
}
	

	
	
	

	
	




package com.codistan.seleniumprojectone;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class SeleniumWebdriverNavigate {

@Test(groups= {"smokeTest","regression"})
@Parameters("url")
public void seleniumNavigate(String urlname) {
		WebDriverManager.chromedriver().setup();
		//re-usability of also settings
		WebDriver driver = new ChromeDriver();
		driver.get("https://www.codistan.com");
		driver.navigate().to("https://www.codistan.com");
        driver.manage().window().maximize();
        //maximazing windows
        //selenium only imitates user`s actions
        driver.manage().window().fullscreen();
        driver.navigate().to("https://www.ebay.com");
        driver.navigate().back();
        //back button on a browser
        driver.navigate().refresh();
        driver.navigate().forward();
        //forward button on a browser
        //singleton pattern, we will use for frame work.
        driver.close();
	}

}



























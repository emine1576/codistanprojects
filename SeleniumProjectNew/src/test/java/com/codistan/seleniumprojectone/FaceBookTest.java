package com.codistan.seleniumprojectone;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class FaceBookTest {

	@Test(groups= {"smokeTest","regression"})
	@Parameters("url")
	public void faceBookT(String urlname) {
		// TODO Auto-generated method stub
		
		
		
		
		
		WebDriverManager.chromedriver().setup();
		WebDriver driver = new ChromeDriver();
		driver.get("https://www.facebook.com");
		WebElement  signoutButton = driver.findElement(By.xpath("//a[@id='u_0_2']"));
		boolean isSignupDisplay = signoutButton.isDisplayed();
		if(isSignupDisplay) {
			System.out.println(" sign up button is displayed");
		}else {
			System.out.println("There is a problem with sign up button");
		}
		WebElement customRadioButton = driver.findElement(By.xpath("//a[@id=\'facebook\']/body"));
		WebElement femaleRadioButton = driver.findElement(By.xpath("//a[@id=\'facebook\']/body"));
		WebElement maleRadioButton = driver.findElement(By.xpath("//a[@id=\'facebook\']/body"));
		WebElement createNewAccountButton = driver.findElement(By.xpath("//a[@id=\'facebook\']/body"));
		createNewAccountButton.click();
		if(femaleRadioButton.isSelected()) {
			System.out.println("Female is selected");
		}else if(maleRadioButton.isSelected()) {
			System.out.println("Male is selected");
		}else if(customRadioButton.isSelected()) {
			System.out.println("Custom is selected");
		}else {
			System.out.println("Nothing is selected");
		}
		driver.close();
	}
	}


package com.codistan.seleniumprojectone;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class SeleniumPracticeExerciseFour {

	public static void main(String[] args) {
		

		
		
		WebDriverManager.chromedriver().setup();
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
		driver.get("https://www.amazon.com");
		String pageSourceOfAmazon = driver.getPageSource();
		int amazonL = pageSourceOfAmazon.length();
		driver.get("https://www.ebay.com");
		String pageSourceOfEbay = driver.getPageSource();
		int ebayL = pageSourceOfEbay.length();
		if (ebayL>amazonL) {
			System.out.println("Ebay html is bigger.");
			
		}else {
			System.out.println("amazon html bigger.");
			
		}
		
	}

}

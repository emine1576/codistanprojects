package com.codistan.seleniumprojectone;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class SeleniumSelect {

	@Test(groups= {"smokeTest","regression"})
	@Parameters("url")
	public void seleniumSe(String urlname) {
		String operatingSystem = System.getProperty("os.name");
		String projectName =System.getProperty("user.dir");
		if(operatingSystem.equalsIgnoreCase("windows10")) {
		
	WebDriverManager.chromedriver().setup();
	WebDriver driver =new ChromeDriver();
	/** If operating system is Windows 10
		 * Then Select "Select Make" as "Acura" by using "Selectby Index"
		 * Then Select "Year" as "2020" by using "SelectbyValue"
		 * Then Select "Select Trim" as "A-spec" by using "SelectbyVisibleText"
		 * Then Select "Miles" as "100 Miles" by using "Selectby Index"
	driver.get("https://www.cargurus.com/Cars/new/");
	*/
	driver.get("https://www.cargurus.com/Cars/new/");
	WebElement selectMakeLocator = driver.findElement(By.xpath("//select[@id=\'submitTopicForm_entitySelectingHelper_selectedEntity_modelSelect\']"));
	Select selector = new Select(selectMakeLocator);
	//selector.selectByIndex(3);
	//selector.selectByVisibleText("Acura");
	selector.selectByValue("m4");
	WebElement selectModelLocator = driver.findElement(By.xpath("//select[id=\'submitTopicForm_entitySelectingHelper_selectedEntity_modelSelect\']"));
	Select selectModel = new Select(selectModelLocator);
	selectModel.selectByIndex(5);
	selectModel.selectByValue("d921");
	//if you can not selected it is not a index
	
	WebElement selectTrimLocator = driver.findElement(By.xpath("//select[@id=\'submitTopicForm_entitySelectingHelper_selectedEntity_trimSelect\']"));
	Select selectTrim = new Select(selectTrimLocator);
	selectTrim.selectByVisibleText("FWD with A-Spec Package ");
	
	
	}
  else {
     System.out.println();
}
}
}
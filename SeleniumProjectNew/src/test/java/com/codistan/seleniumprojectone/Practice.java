package com.codistan.seleniumprojectone;

import java.util.Random;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Practice {

	public static void main(String[] args) {
		//Random r = new Random();
		//int number = r.nextInt(3)+1;
		//System.out.println(number);
		
WebDriverManager.chromedriver().setup();
WebDriver driver = new ChromeDriver();
driver.manage().window().maximize();
driver.get("http://secure.smartbearsoftware.com/samples/TestComplete11/WebOrders/Process.aspx");
WebElement UserNameTextField =driver.findElement(By.xpath("//input[@id='ctl00_MainContent_username']"));
UserNameTextField.sendKeys("Tester");
WebElement PasswordTextField = driver.findElement(By.xpath("//input[@id='ctl00_MainContent_password']"));
PasswordTextField.sendKeys("test");
WebElement LoginButton =driver.findElement(By.xpath("//input[@id='ctl00_MainContent_login_button']"));
LoginButton.click();
WebElement selectProductLocator = driver.findElement(By.xpath("//select[@id='ctl00_MainContent_fmwOrder_ddlProduct']"));
Select selectProduct = new Select(selectProductLocator);
selectProduct.selectByValue("MyMoney");
WebElement QuantitiyField = driver.findElement(By.xpath("//input[@id='ctl00_MainContent_fmwOrder_txtQuantity']"));
QuantitiyField.clear();
QuantitiyField.sendKeys("4");
WebElement TotalField = driver.findElement(By.xpath("//input[@id='ctl00_MainContent_fmwOrder_txtTotal']"));
TotalField.click();
WebElement CustomerName= driver.findElement(By.xpath("//input[@id='ctl00_MainContent_fmwOrder_txtName']"));
CustomerName.sendKeys("kate");
WebElement StreetField = driver.findElement(By.xpath("//input[@id='ctl00_MainContent_fmwOrder_TextBox2']"));
StreetField.sendKeys("avenue D");
WebElement CityNameField = driver.findElement(By.xpath("//input[@id='ctl00_MainContent_fmwOrder_TextBox3']"));
CityNameField.sendKeys("lodi");
WebElement StateNameField = driver.findElement(By.xpath("//input[@id='ctl00_MainContent_fmwOrder_TextBox4']"));
StateNameField.sendKeys("new jersey");
WebElement ZipCodeField  = driver.findElement(By.xpath("//input[@id='ctl00_MainContent_fmwOrder_TextBox5']"));
ZipCodeField.sendKeys("07644");

Random r = new Random();
int randomNumber = r.nextInt(3)+1;
String creditCardNumber = "123456789012345";
WebElement cardField = driver.findElement(By.xpath("//input[@id='ctl00_MainContent_fmwOrder_TextBox6']"));
WebElement expirationDate = driver.findElement(By.xpath("//input[@id='ctl00_MainContent_fmwOrder_TextBox1']"));
if(randomNumber==1) {
	WebElement visaButton = driver.findElement(By.xpath("//input[@id='ctl00_MainContent_fmwOrder_cardList_0']"));
	visaButton.click();
	cardField.sendKeys("4" + creditCardNumber);
} else if (randomNumber == 2) {
	WebElement masterCardPaymentButton = driver
			.findElement(By.xpath("//input[@id='ctl00_MainContent_fmwOrder_cardList_1']"));
	masterCardPaymentButton.click();
	cardField.sendKeys("5" + creditCardNumber);
} else {
	WebElement americanExpressPaymentButton = driver
			.findElement(By.xpath("//input[@id='ctl00_MainContent_fmwOrder_cardList_2']"));
	americanExpressPaymentButton.click();
	cardField.sendKeys(creditCardNumber);
}
expirationDate.sendKeys("02/22");
}

	



}





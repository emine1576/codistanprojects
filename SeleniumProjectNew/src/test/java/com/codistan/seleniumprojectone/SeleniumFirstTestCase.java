package com.codistan.seleniumprojectone;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class SeleniumFirstTestCase {

	@Test(groups= {"smokeTest,regression"})
	@Parameters("url")
	public void seleniumFirstCase(String urlname) {
		
		////How to invoke (launch) a browser 
		//Multiple imports you can use ctrl +shift + o
		WebDriverManager.firefoxdriver().setup();
		WebDriverManager.chromedriver().setup();
		//Java has convention rules,syntax.These specific words are reserved.
		
		WebDriver driver = new ChromeDriver();
		//web driver is an interface(ready to (abstract) methods for you to use it.
		
		String codistanUrl ="https://www.codistan.com";//literal string
		
		driver.get(codistanUrl);
		String amazonUrl = "https://www.com.codistan.com";
		driver.get(amazonUrl);
		String getTitle = driver. getTitle();
		//$ and _are acceptable as a java variable name
		//getTitle is not java naming Title
		
        driver.get("https://www.ebay.com");
        System.out.println(driver.getTitle());
		System.out.println(driver.getCurrentUrl());
        

	}

}

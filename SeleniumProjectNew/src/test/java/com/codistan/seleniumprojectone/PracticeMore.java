package com.codistan.seleniumprojectone;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import io.github.bonigarcia.wdm.WebDriverManager;

public class PracticeMore {

	public static void main(String[] args) {
		WebDriverManager.chromedriver().setup();
		WebDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("https://reverb.com/sell?blank");
		WebElement brandTextField = driver.findElement(By.xpath("//input[@id='product_make']"));
		brandTextField.sendKeys("Fender");
		WebElement modelTextField = driver.findElement(By.xpath("//input[@id='product_model']"));
		modelTextField.sendKeys("Stratocaste");
		WebElement conditionDropdown = driver.findElement(By.xpath("//select[@id='product-condition']"));
		Select selectCondition = new Select(conditionDropdown);
		selectCondition.selectByValue("excellent");
		WebElement yearTextField = driver.findElement(By.xpath("//input[@id='product_year']"));
		yearTextField.sendKeys("2010");
		WebElement color = driver.findElement(By.xpath("//input[@id='product_finish']"));
		color.sendKeys("Blue");
		WebElement listingTitle = driver.findElement(By.xpath("//input[@id='product_title']"));
		listingTitle.sendKeys("Fender Stratocaster 2010 Blue");
		WebElement listingPrice = driver.findElement(By.xpath("//input[@id='product_price']"));
		listingPrice.sendKeys("100");
		WebElement countryLocator = driver.findElement(By.xpath("//*[@id=\"product_country_of_origin_chzn\"]/a/span"));
		countryLocator.click();
		WebElement countryLocatorTextField = driver
				.findElement(By.xpath("//*[@id=\"product_country_of_origin_chzn\"]/div/div/input"));
		countryLocatorTextField.sendKeys("United States" + Keys.ENTER);
		WebElement offersCheckBox = driver.findElement(By.xpath("//input[@id='product_offers_enabled']"));

 
 
}

}

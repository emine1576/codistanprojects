package com.codistan.seleniumprojectone;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

@Test
public class SeleniumDriverNull {

@Parameters("url")
public void seleniumDriverN(String urlname) {
    WebDriver driver = null;
    
    if(driver == null) {
	WebDriverManager.chromedriver().setup();
	driver = new ChromeDriver();
	WebDriverManager.firefoxdriver().setup();
	driver = new FirefoxDriver();
	driver.get("https://www.google.com");
	driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);
	
	/*implicit wait can handle %95 wait related issues.
	 * Selenium Web driver by default assumes all elements are present when you invoke
	 * browser and open page
	 * 
	 * 
	 * 
	 * 
	 * 
	 * 
	 */
	
	
	
		}
		

    
	}

}

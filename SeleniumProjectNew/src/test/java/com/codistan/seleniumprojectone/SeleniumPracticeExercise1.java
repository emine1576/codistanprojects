package com.codistan.seleniumprojectone;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class SeleniumPracticeExercise1 {

	@Test(groups= {"smokeTest,regression"})
	@Parameters("url")
	public void seleniumPrtExer(String urlname) {
		//user stories keep in jira, test cases in zephyr
		/*When user opens a browser
		 * and user goes amazon.com
		 *  when user enter "fidget spinner" to search box and press +enterKeys.ENTER
		 * then user should see "fidget spinner" search result
		 * 
		 * 
		 * 
		 * 
		 * 
		 * 
		 * 
		 * 
		 */
		WebDriverManager.chromedriver().setup();
		WebDriver driver = new ChromeDriver();
		
		String amazonUrl = "https://www.amazon.com";
		driver.get(amazonUrl);
		driver.manage().window().maximize();
		String twoTab ="twotabsearchtextbox";
		WebElement searchBox = driver.findElement(By.id(twoTab));
		//for locators we have Webelement type , we can not use String.
		String fs = "fidget spinner";
	   
		searchBox.sendKeys(fs+"fidget spinner");
		driver.findElement(By.id("twotabsearchtextbox")).sendKeys();
       //driver.findElements(By.id("twotabsearchtextbox")).sendKeys("fidget spinner"+Keys.ENTER); 
	}

}

package com.codistan.seleniumprojectone;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import io.github.bonigarcia.wdm.WebDriverManager;

public class OrbitzExercise {

	@Test(groups= {"smokeTest,regression"})
	@Parameters("url")
	public void orbitzExercise(String urlname) {
   WebDriverManager.chromedriver().setup();
   WebDriver driver =new ChromeDriver();
driver.get("https://www.orbitz.com/");
		driver.navigate().to("https://www.orbitz.com/");
        driver.manage().window().maximize();
        WebElement originCity = driver.findElement(By.xpath("//input[@id=\'package-origin-hp-package\']"));
     originCity.sendKeys("chicago");
    WebElement departCity = driver.findElement(By.xpath("//input[@id=\'package-destination-hp-package\']"));
	departCity.sendKeys("istanbul");
	WebElement departDate =driver.findElement(By.xpath("//input[@id='package-departing-hp-package']"));
	departDate.sendKeys("01/05/2020");
	WebElement returnDate =driver.findElement(By.xpath("//input[@id='package-returning-hp-package']"));
	returnDate.sendKeys("01/25/2020");
	WebElement selectRoomLocator = driver.findElement(By.xpath("//select[@id='package-rooms-hp-package']"));
	Select selectRoom = new Select(selectRoomLocator);
	selectRoom.selectByValue("3");
	WebElement selectRoom1Locator = driver.findElement(By.xpath("//select[@id='package-1-adults-hp-package']"));
	Select selectRoom1 = new Select(selectRoom1Locator);
	selectRoom1.selectByValue("1");
	WebElement selectRoom2Locator =driver.findElement(By.xpath("//select[@id='package-2-adults-hp-package']"));
	Select selectRoom2 = new Select(selectRoom2Locator);
	selectRoom2.selectByValue("1");
	WebElement selectRoom3AdultLocator = driver.findElement(By.xpath("//select[@id='package-3-adults-hp-package']"));
	Select selectRoom3Adult = new Select(selectRoom3AdultLocator);
	selectRoom3Adult.selectByValue("1");
	WebElement selectRoom3ChildrenLocator =driver.findElement(By.xpath("//select[@id='package-3-children-hp-package']"));
	Select selectRoom3Children =new Select(selectRoom3ChildrenLocator);
	selectRoom3Children.selectByValue("1");
	WebElement selectChild1AgeLocator =driver.findElement(By.xpath("//select[@id='package-3-age-select-1-hp-package']"));
	Select selectChild1Age =new Select(selectChild1AgeLocator);
	selectChild1Age.selectByValue("15");
	driver.findElement(By.xpath("//button[@id='search-button-hp-package']")).click();
	

	
	
	
	}     
	
	
	
	

}

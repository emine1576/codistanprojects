package Projects.CodistanProjects;

public class OverLoading {
	public void sum(int a, int b) {
		System.out.println("sum is" +(a+b));
	}
	public void sum(double a, double b) {
		System.out.println("sum is" +(a+b));
	}
	public static void main (String[] args)
	  {
	    OverLoading  over = new OverLoading();
	    over.sum (8,5);      //sum(int a, int b) is method is called.
	    over.sum (4.6d, 3.8d); //sum(double a, double b) is called.
}
	
}
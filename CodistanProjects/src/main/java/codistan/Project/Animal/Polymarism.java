package codistan.Project.Animal;

public class Polymarism {

	public static void main(String[] args) {
		Animal myAnimal = new Animal();
	    Animal myCat = new Cat();
	    Animal myDog = new Dog();
	    Animal myCow = new Cow();
	    
	        
	    myAnimal.animalSound();
	    myCat.animalSound();
	    myDog.animalSound();
	    myCow.animalSound();
	}

}

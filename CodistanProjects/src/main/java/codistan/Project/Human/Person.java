package codistan.Project.Human;

import codistan.Project.Animal.Dog;

public class Person {
	  public void dance() {
	        Dog dog = new Dog();
	       // dog.waveTail(); // The waveTail() method of the Dog class is protected, thus the Person class cannot invoke it:
	        //compile  error
	        
	       // public void play() {
	            //Cat wolf = new Cat(); // COMPILE ERROR because it is different package.
	    }
	  private void read() {
		  System.out.println("person can read");
	  }
	}


package codistan.java.collections;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.ListIterator;

public class ArrayListsAndIterator {
	public static void main(String[] args) {
		ArrayList<String> fruits = new ArrayList<String>();
		fruits.add("apple");
		fruits.add("grape");
		fruits.add("banana");
		fruits.add("strawberry");
		fruits.add("lime");
		System.out.println(fruits);
		fruits.get(0);
		fruits.get(1);
		fruits.get(2);
		fruits.get(3);
		fruits.get(4);
		ListIterator<String> listItr = fruits.listIterator();
		 
		while(listItr.hasNext())
		{
		    System.out.println(listItr.next());
		}
		
		 Collections.sort(fruits);

		   /* Sorted List*/
		   System.out.println("After Sorting:");
		   for(String counter: fruits){
				System.out.println(counter);
	}
		   
	
		   
}
	
			
}

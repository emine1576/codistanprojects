package codistan.java.collections;

import java.util.TreeSet;

public class Treeset {

	public static void main(String[] args) {
		 TreeSet<String> tset = new TreeSet<String>();
		 tset.add("pencil");
		 tset.add("pen");
		 tset.add("ink");
		 tset.add("eraser");
		 tset.add("notebook");
		 tset.add("book");
		// tset.add(null);not allowed
		// tset.add("book"); not allowed
		 System.out.println(tset);
		 
		 TreeSet<Integer> tset2 = new TreeSet<Integer>();
         tset2.add(5);
         tset2.add(0);
         tset2.add(1);
         tset2.add(4);
         tset2.add(3);
         tset2.add(2);
         System.out.println(tset2);
         
         

	}

}

package com.codistan.seleniumframework;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

import io.github.bonigarcia.wdm.WebDriverManager;

public class AmazonTestcaseTwo {
	public static WebDriver driver;

	public static void main(String[] args) {
		WebDriverManager.chromedriver().setup();
		driver = new ChromeDriver();
		driver.get("http://www.amazon.com");
		driver.manage().window().maximize();
		Actions act = new Actions(driver);
		act.moveToElement(AmazonLocators.amazonAccountAndLists(driver)).click(AmazonLocators.amazonFindAGift(driver)).build().perform();
	    
}
}
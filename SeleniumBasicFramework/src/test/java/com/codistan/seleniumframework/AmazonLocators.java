package com.codistan.seleniumframework;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class AmazonLocators {
	
public static WebElement element;
//POM(page object modelling), design purpose of our test case
//purpose is usability and maintaince
	public static WebElement amazonSearchBar(WebDriver driver) {
		
		return element = driver.findElement(By.xpath("//input[@id='twotabsearchtextbox']"));
		//return element;
	}
 public static WebElement amazonAccountAndLists(WebDriver driver) {
	 return element = driver.findElement(By.xpath("//span[contains(@class,'nav-line-2')][contains(text(),'Account & Lists')]")); 

 }
 public static WebElement amazonFindAGift(WebDriver driver) {
	 return element = driver.findElement(By.xpath("//span[contains(text(),'Find a Gift')]")); 
		
}	}
	




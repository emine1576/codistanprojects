@sendform @sprint
Feature: Filling our Contact Form
 Scenario Outline: Open avalonsolus.com/contact/ and fill out contact form
    When User opens contact page
    And User fills "<name>"  "<email>"  "<subject>"  "<message>" 
    Then User for should be sent
    Examples: 
      | name | email | subject | message |
      | ty   | arsu  | info    | textme  |

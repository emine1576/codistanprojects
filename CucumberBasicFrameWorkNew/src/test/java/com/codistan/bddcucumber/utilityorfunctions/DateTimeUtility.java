package com.codistan.bddcucumber.utilityorfunctions;


import java.text.SimpleDateFormat;
import java.util.Date;

public class DateTimeUtility {
	
public static String currentTime() {
		
		String currentDate = new SimpleDateFormat("MMMM dd, yyy").format(new Date());
		
		return currentDate;
		
	}
	//dd-MM-yy   06-02-20
	
	//We use this not to make duplicate screenshot and reports.
	//We run smoke test in the first build more, to check app stability and health.
	//Screenshot and reports we keep them in our server. QA Manager take care of it.



}

package com.codistan.bddcucumber.utilityorfunctions;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class BrowserFunctionMethods {
	
	/*
	 * How do you pause test execution? Thread.sleep(3000); Static method, we do not
	 * create an object of it. Thread trd = new Thread();
	 * 
	 * Main method is also static, Main mn = new Main();
	 * 
	 * Wait Types
	 * 
	 * 
	 */

	// Create a method that handles thread.sleep with exception, method name is
	// threadWait

	public static void threadWait() {

		try {
			// Try to do this
			Thread.sleep(3000);

		} catch (InterruptedException e) {
			// If you can not do try block, then run catch block

			e.printStackTrace();

			// Stack ExceptionTrace with System error.

		}

	}

	/*
	 * ExplicitMethod A method that will wait for clickability of web element
	 * 
	 * 
	 */

	public static WebElement waitForClickabilityByWebElement(WebElement element, int timeOut) {

		// Is specified for each webelement different, custom.
		WebDriverWait wait = new WebDriverWait(ProjectDriver.getDriverReference(), timeOut);

		return wait.until(ExpectedConditions.elementToBeClickable(element));

	}

	/*
	 * ExplicitMethod A method that will wait for clickability of web element(By
	 * locator)
	 * 
	 */

	public static WebElement waitForClickabilityByLocator(By locator, int timeOutLocator) {

		// Is specified for each webelement different, custom.
		WebDriverWait wait = new WebDriverWait(ProjectDriver.getDriverReference(), timeOutLocator);

		return wait.until(ExpectedConditions.elementToBeClickable(locator));

	}

	/*
	 * Explicit Visibility
	 * 
	 * 
	 * 
	 */

	/*
	 * JavaScriptExecutor JavaScriptExecutor is an alternative way of sending keys,
	 * click, scroll down...
	 * 
	 * ***Selenium do not work really well with Ajax, Angular and Node JS.
	 * 
	 * Selenium disadvantage-->Node JS ---> Cypress (Testing Tool)
	 * 
	 * Chi ---> Chicago
	 * 
	 * Ajax is a dream development tool for web developers, without saving data to
	 * web server we can handle some of these stuff on web page. Ajax enables us to
	 * update a webpage without reloading the page Send data to webserver in the
	 * background
	 * 
	 */
	
	/*
	 * 
	 * ClickwithJavaScript
	 * 
	 * 
	 */
	
	
	public static void clickWithJavaScriptExecutor(WebElement element) {
		
		JavascriptExecutor js = (JavascriptExecutor) ProjectDriver.getDriverReference();
		
		js.executeScript("argument[0].click();", element);
		
	}
	
	
	/*
	 * 
	 * SendKeys with JavaScript
	 * This an alternative way of sending keys with JavaScriptExecutor
	 * 
	 * 
	 */
	
	public static void sendKeysWithJavaScriptExecutor() {
		
		JavascriptExecutor js = (JavascriptExecutor) ProjectDriver.getDriverReference();
		
		js.executeScript("document.getElementById('some id').value='someValue';");
		
	}
	
	/*
	 * 
	 * Scroll Down to An Element
	 * If the application is running in Ajax, Angular(Sendkeys
	 * 
	 * 
	 */
	
	public static void scrollDownWithJavaScriptExecutor() {
		
		JavascriptExecutor js = (JavascriptExecutor) ProjectDriver.getDriverReference();
		js.executeScript("window.scrollBy(0,500)");
		
		
	}
	
	


}

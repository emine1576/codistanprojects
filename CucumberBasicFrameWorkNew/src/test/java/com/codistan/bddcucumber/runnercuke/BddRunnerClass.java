package com.codistan.bddcucumber.runnercuke;

import org.junit.runner.RunWith;

import io.cucumber.junit.Cucumber;
import io.cucumber.junit.CucumberOptions;

@RunWith(Cucumber.class)

//To run it as a cucumber class, means run it with cucumber.junit and cucumber.java dependencies
//Main method(Java Application), with TestNG TestNG Test

@CucumberOptions(features = "src\\test\\resources\\Features", glue = { "com.codistan.bddcucumber.stepdefinitions" },

		plugin = { "pretty", "json:target/cucumber-reports/cucumber.json",
				"junit:target/cucumber-reports/Cucumber.xml"})

/*
* 1-json Jenkins CI server, Cucumber report Jenkins reads 2-xml 3-html
* 
*/

//I show location of my feature folder, and stepdefinitions package



public class BddRunnerClass {
	
}

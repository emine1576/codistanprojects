package com.codistan.bddcucumber.stepdefinitions;

import com.codistan.bddcucumber.utilityorfunctions.ProjectDriver;

import io.cucumber.java.After;
import io.cucumber.java.Before;

public class HookClass {

	@Before 
	
	public void setUpCucumberDriver() {
		
		ProjectDriver.startDriver();
		
		
	}
	@After
	
	public void endTest() {
		
		//Screenshot...
		
	//    @After
	//    public void teardown(Scenario scenario){
//	        //if test failed - do this
//	        if(scenario.isFailed()){
//	            logger.error("Test failed!");
//	            byte[] screenshot = ((TakesScreenshot)Driver.get()).getScreenshotAs(OutputType.BYTES);
//	            scenario.embed(screenshot, "image/png");
//	        }else{
//	            logger.info("Cleanup!");
//	            logger.info("Test completed!");
//	        }
//	        logger.info("##############################");
//	        //after every test, we gonna close browser
//	        Driver.close();
//	    }
	}
		

}

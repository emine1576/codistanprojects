package com.codistan.bddcucumber.stepdefinitions;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;

import com.codistan.bddcucumber.utilityorfunctions.ProjectDriver;
import com.codistan.bddcucumber.utilityorfunctions.PropertyConfig;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class ContactFormScenarioOutline {
	@When("User opens contact page")
	public void user_opens_contact_page() {
		
		ProjectDriver.getDriverReference().get(PropertyConfig.getProperty("urlavalon"));
		
		// Properties file works in key and value pair.
		// Properties is a subclass of Hastable. Map also works in key and value pair, Parameters also works in Key and Value pair. 
	   
	}

	@When("User fills {string}  {string}  {string} {string}")
	public void user_fills(String string, String string2, String string3, String string4) {
		
		WebElement nameField= ProjectDriver.getDriverReference().findElement(By.xpath("//input[@placeholder='Name *']"));
		WebElement emailField= ProjectDriver.getDriverReference().findElement(By.xpath("//input[@placeholder='Email *']"));
		WebElement subjectField= ProjectDriver.getDriverReference().findElement(By.xpath("//input[@placeholder='Subject *']"));
		WebElement messageField= ProjectDriver.getDriverReference().findElement(By.xpath(("//textarea[@placeholder='Message *']")));
		WebElement submitButton = ProjectDriver.getDriverReference().findElement(By.xpath(("//*[@id=\"wpcf7-f8927-p87-o1\"]/form/p[5]/input")));
//		nameField.sendKeys(string);
//		emailField.sendKeys(string2);
//		subjectField.sendKeys(string3);
//		messageField.sendKeys(string4);
		
		
		
		JavascriptExecutor jse = (JavascriptExecutor) ProjectDriver.getDriverReference();
		
		jse.executeScript("arguments[0].value='"+string+"';", nameField);
		jse.executeScript("arguments[0].value='"+string2+"';", emailField);
		jse.executeScript("arguments[0].value='"+string3+"';", subjectField);
		jse.executeScript("arguments[0].value='"+string4+"';", messageField);
		jse.executeScript("argument[0].click();", submitButton);
		
		
		
		
		
		
	   
	}

	@Then("User for should be sent")
	public void user_for_should_be_sent() {
		
		//Then keyword is the result part!
		System.out.println("We can use assertion here");
		
		
		
	    
	}

	
	

}
